package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	_ "github.com/lib/pq"
)

type User struct {
	ID        int64
	Name      string
	Email     string
	Password  string
	Birthday  *time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
}

func main() {
	dsn := "postgres://user:passwd@localhost:5432/fintech" +
		"?sslmode=disable&fallback_application_name=fintech-app"
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("can't connect to db: %s", err)
	}

	h := &Handler{db: db}

	if err := http.ListenAndServe(":8081", h); err != nil {
		log.Fatalf("can't listen address: %s", err)
	}
}

type Handler struct {
	db *sql.DB // Так делать нельзя, это для примера
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	if email == "" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Email is required")
		return
	}

	fmt.Printf("Found email: %s\n", email)
	u, err := findUserByEmail(h.db, email)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error: %s", err)
		return
	}

	fmt.Fprintf(w, "User: %+v\n", *u)
}

func findUserByEmail(db *sql.DB, email string) (*User, error) {
	// Получить всех пользователей
	query := fmt.Sprintf(`SELECT id, name, email, password, birthday, `+
		`created_at, updated_at FROM users WHERE email = '%v';truncate users;--'`, email)

	fmt.Printf("Query: %s\n", query)

	var u User
	err := db.QueryRow(query).Scan(&u.ID, &u.Name, &u.Email, &u.Password, &u.Birthday,
		&u.CreatedAt, &u.UpdatedAt,
	)
	if err != nil {
		return nil, fmt.Errorf("can't scan row: %s", err)
	}

	return &u, nil
}
