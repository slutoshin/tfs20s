package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func Handler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Printf("can't upgrade connection: %s\n", err)
		return
	}

	for n := 0; n < 10; n++ {
		msg := "hello  " + string(n+48)
		fmt.Printf("sending to client: %s\n", msg)
		err = conn.WriteMessage(websocket.TextMessage, []byte(msg))
		_, reply, err := conn.ReadMessage()
		if err != nil {
			fmt.Printf("can't receive: %s\n", err)
		}
		fmt.Printf("received back from client: %s\n", string(reply[:]))
	}
	conn.Close()
}

func main() {
	http.HandleFunc("/", Handler)
	err := http.ListenAndServe("localhost:5000", nil)
	if err != nil {
		log.Fatalf("can't start server: %s", err)
	}
}
