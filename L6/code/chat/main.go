package main

import (
	"github.com/gorilla/websocket"
	"time"
	"log"
	"net"
	"html/template"
	"net/http"
	"sync"
)


var templates map[string]*template.Template

func init() {
	if templates == nil {
		templates = make(map[string]*template.Template)
	}
	templates["index"] = template.Must(template.ParseFiles("html/index.html", "html/base.html"))
}

type Chat struct {
	mutex  sync.Mutex
	nextID int
	users  map[int]*websocket.Conn
}

func (c *Chat) AddUser(conn *websocket.Conn) int {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	c.nextID++
	c.users[c.nextID] = conn
	return c.nextID
}

func (c *Chat) RemoveUsers(IDs ...int) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	for _, id := range IDs {
		c.users[id].Close()
		delete(c.users, id)
	}
}

func (c *Chat) Broadcast(msg [][]byte) {
	c.mutex.Lock()
	inactiveUsers := make([]int, 0)
	for id, conn := range c.users {
		if err := conn.WriteMessage(websocket.TextMessage, msg); err != nil {
			log.Println(err)
			inactiveUsers = append(inactiveUsers, id)
		}
	}
	c.mutex.Unlock()
	c.RemoveUsers(inactiveUsers...)
}

func (c *Chat) Do(w http.ResponseWriter, r *http.Request) {
	var up websocket.Upgrader // set buffers
	conn, err := up.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	conn.SetPingHandler(func(appData string)error {
		err := conn.WriteControl(websocket.PongMessage, []byte(appData), time.Now().Add(time.Second * 1))
			if err == websocket.ErrCloseSent {
				return nil
			} else if e, ok := err.(net.Error); ok && e.Temporary() {
				return nil
			}
			return err
	})
	userID := c.AddUser(conn)
	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			if err == io.EOF{
				log.Println(err)
				c.RemoveUsers(userID)
				return
			}
		}
		с.db.save()
		//c.Broadcast(msg)
	}
}

func renderTemplate(w http.ResponseWriter, name string, template string) {
	tmpl, ok := templates[name]
	if !ok {
		http.Error(w, "can't find template", http.StatusInternalServerError)
	}
	err := tmpl.ExecuteTemplate(w, template, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func RenderChat(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "index", "base")
}


func main() {
	c := &Chat{
		users : make(map[int]*websocket.Conn),
		mutex : sync.Mutex{},
	}
	http.HandleFunc("/", RenderChat)
	http.HandleFunc("/chat", c.Do)
	if err := http.ListenAndServe(":5000", nil); err != nil {
		log.Fatal(err)
	}
}
