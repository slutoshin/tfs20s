package main

import (
	"log"
	"os"
	"text/template"
)

type Candle struct {
	Name       string
	O      	   float64
	H          float64
	C          float64
	L          float64
}

const tmpl = `{{$a := 100.0}}{{if eq $a .O}}O is 100{{end}}`

func main() {
	aapl := Candle{"aapl", 100.0,100.0,100.0,100.0}
	t := template.New("aapl")
	t, err := t.Parse(tmpl)
	if err != nil {
		log.Fatal("can't parse: ", err)
	}
	if err := t.Execute(os.Stdout, aapl); err != nil {
		log.Fatal("can't execute: ", err)
	}
}
