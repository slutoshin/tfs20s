package main

import (
	"github.com/go-chi/chi"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

type Candle struct {
	Name       string
	O      	   float64
	H          float64
	C          float64
	L          float64
}

var candles = []Candle{
	{"AAPL", 100.0, 100.0,100.0, 100.0},
	{"AMZN", 105.0, 105.0,105.0, 105.0},
	{"YNDX", 156.56, 156.56,156.56,156.56},
}

var templates map[string]*template.Template

func init() {
	if templates == nil {
		templates = make(map[string]*template.Template)
	}
	templates["index"] = template.Must(template.ParseFiles("html/index.html", "html/base.html"))
	templates["updprice"] = template.Must(template.ParseFiles("html/updprice.html", "html/base.html"))
}

func renderTemplate(w http.ResponseWriter, name string, template string, viewModel interface{}) {
	tmpl, ok := templates[name]
	if !ok {
		http.Error(w, "can't find template", http.StatusInternalServerError)
	}
	err := tmpl.ExecuteTemplate(w, template, viewModel)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func GetCandles(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "index", "base", candles)
}

func GetUpdateCandlePrice(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, "can't find candle by ID", http.StatusBadRequest)
		return
	}

	if id > len(candles) {
		http.Error(w, "can't find lot by ID", http.StatusBadRequest)
		return
	}
	candle := candles[id]

	renderTemplate(w, "updprice", "base", candle)
}

func PostUpdateCandlePrice(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, "can't find candle by ID", http.StatusBadRequest)
		return
	}

	if id > len(candles) {
		http.Error(w, "can't find candle by ID", http.StatusBadRequest)
		return
	}

	newPrice, err := strconv.ParseFloat(r.PostFormValue("newPrice"), 64)
	if err != nil {
		http.Error(w, "can't find new price", http.StatusBadRequest)
		return
	}

	candles[id].O = newPrice

	http.Redirect(w, r, "/market", 302)
}

func main() {
	r := chi.NewRouter()

	r.Route("/market", func(r chi.Router) {
		r.Get("/", GetCandles)
		r.Get("/candles/{id}/updprice", GetUpdateCandlePrice)
		r.Post("/candles/{id}/updprice", PostUpdateCandlePrice)
	})

	if err := http.ListenAndServe(":5000", r); err != nil {
		log.Fatal(err)
	}
}
