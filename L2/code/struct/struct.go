package main

import (
	"fmt"
	"strings"
	"time"
	"unsafe"
)

type Exchange struct {
	Name      string    `json:"Exchange"`
	StartTime time.Time `json:"start_time"`
	EndTime   time.Time `json:"end_time"`
	stocks    []string  // это поле не может быть использовано в других пакетах
}

type Candle struct {
	o    float64
	h    float64
	l    float64
	c    float64
	name string
}

func compare() {
	c1 := Candle{o: 63.2, h: 64.6, l: 61.2, c: 61.2, name: "AAPL"}
	c2 := c1
	if c1 == c2 {
		c2.name = "SBER"
		fmt.Printf("C1 : %+v\nC2 : %+v", c1, c2)
	}
}

func updatePriceByValue(c Candle) {
	c.h += 10
}

func updatePriceByRefernce(c *Candle) {
	c.h += 10
}

func update() {
	c1 := Candle{o: 63.2, h: 64.6, l: 61.2, c: 61.2, name: "AAPL"}
	updatePriceByValue(c1)
	fmt.Println(c1.h)
	updatePriceByRefernce(&c1)
	fmt.Println(c1.h)
}

func (e *Exchange) updateStartTime(d time.Duration) {
	e.StartTime = e.StartTime.Add(d) // сигнатура Add - func(t Time) Add (d time.Duration) time.Time
}

type User struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	email     string
	Company   string `json:"company"`
}

// В Go нет конструкторов, но есть функции, которыми можно создавать объекты.
// Перед созданием объект можно валидировать
func NewUser(firstName, lastName, email, company string) (*User, error) {
	if err := validateEmail(email); err != nil {
		return nil, fmt.Errorf("can't validarte email %q: %s", email, err)
	}

	user := User{
		FirstName: firstName,
		LastName:  lastName,
		email:     email,
		Company:   company,
	}

	return &user, nil
}

// Объект по умолчанию
func NewAnonymousUser() User {
	return User{
		FirstName: "User",
		email:     "anonym@tinkoff.ru",
	}
}

// Метод на объекте, не изменяет состояние объекта
func (u User) IsStaff() bool {
	return u.Company == "Tinkoff"
}

// Метод на указателе на объект, изменяет состояние объекта
func (u *User) ChangeEmail(email string) error {
	if err := validateEmail(email); err != nil {
		return fmt.Errorf("can't validate email: %s", err)
	}

	u.email = email
	return nil
}

// Функция изменяем состояние объекта
func ChangeUserEmail(user *User, email string) error {
	if err := validateEmail(email); err != nil {
		return fmt.Errorf("can't validate email: %s", err)
	}

	user.email = email
	return nil
}

func validateEmail(email string) error {
	if !strings.Contains(email, "@") {
		return fmt.Errorf("bad email format")
	}

	return nil
}

// Встраивание структур
// Теперь все методы
type Admin struct {
	User
	permissions map[string]bool
}

func NewAdmin(user User) Admin {
	return Admin{
		User:        user,                  // Можно здесь перечислить все поля пользователя, если на входе нет объекта User
		permissions: make(map[string]bool), // Инициализация map-ы
	}
}

func (a Admin) CanBlockUser() bool {
	return a.permissions["block_user"]
}

func empty() {
	size := unsafe.Sizeof([10000]struct{}{})
	fmt.Printf("%d\n", size)
}
