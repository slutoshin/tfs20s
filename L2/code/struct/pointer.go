package main

import (
	"encoding/json"
	"log"
	"strings"
	"time"
)

type Quote struct {
	Ask float64
	Bid float64
	Ts  CustomTs
}

type CustomTs struct {
	time.Time
}

// удовлетворяет интерфейсу - подробнее по https://golang.org/pkg/encoding/json/
func (c CustomTs) UnmarshalJSON(data []byte) error { // но будет работать с Pointer
	timeString := strings.Trim(string(data), "\"")
	t, err := time.Parse("15:04", timeString)
	if err != nil {
		return err
	}
	c.Time = t
	return nil
}

func main() {
	data := []byte(`{"Ask":64.2, "Bid":66.6, "Ts":"18:59"}`)
	var q Quote
	if err := json.Unmarshal(data, &q); err != nil {
		log.Fatal(err)
	}

	log.Printf("%+v", q)
}
