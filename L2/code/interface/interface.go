package main

import "fmt"

type Namer interface {
	Name() string
}

func Execute(i Namer) {
	fmt.Println(i.Name())
}

type Admin struct {
	name string
}

func (a *Admin) Name() string {
	return a.name
}
func main() {
	a := Admin{name: "Oleg"}
	Execute(a) // как исправить ?
}
