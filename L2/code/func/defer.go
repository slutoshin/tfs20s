package main

import "fmt"

func PrintReverse() {
	for i := 0; i < 10; i++ {
		defer func(value int) {
			fmt.Printf("%d ", value)
		}(i)
	}
	fmt.Println("End PrintReverse")
}

func DeferContext() {
	for i := 0; i < 10; i++ {
		defer func(value *int) {
			fmt.Printf("%d ", *value)
		}(&i)
	}
}

func main() {

	DeferContext()

	fmt.Println()

	PrintReverse()
}

