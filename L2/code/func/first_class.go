package main

import (
	"fmt"
	"time"
)

// Примеры показывают, что в Go функция является объектом первого класса.
func main() {
	// Функцию можно присвоить переменной
	printHello := func() {
		fmt.Println("Hello world")
	}

	// Вызвать её
	printHello()

	// printHello - это переменная, значит, её можно переопределить
	printHello = func() {
		fmt.Println("Привет, мир!")
	}

	// И снова вызвать
	printHello()

	// Анонимная функция и её вызов
	func() {
		fmt.Println("Anonymous function")
	}()

	// Анонимную функцию можно передать в функцию, которая принимает функцию
	PrintFunc(func() string {
		return "Fintech school"
	})
}

func PrintFunc(f func() string) {
	result := f()
	fmt.Printf("Result: %s\n", result)
}

func IsWork(startTime, endTime time.Time) func(now time.Time)bool {
	return func(now time.Time) bool {
		if now.After(startTime) && now.Before(endTime) {
			return true
		}

		return false
	}
}
