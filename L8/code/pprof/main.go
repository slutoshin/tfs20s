package main

import(
	"encoding/json"
	"net/http"
	"io/ioutil"
	"log"
	"regexp"
	"sync"
	_ "net/http/pprof"
)

type User struct {
	Email string `json:"email"`
	Password string `json:"password"`
}

type UserService struct {
	reg *regexp.Regexp
	mutex sync.Mutex
	users map[string]string
}

func (u *UserService) Create(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var user User
	err = json.Unmarshal(body, &user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	valid := u.reg.Match([]byte(user.Email))
	if valid {
		u.mutex.Lock()
		u.users[user.Email] = user.Password
		u.mutex.Unlock()
	}

	w.WriteHeader(http.StatusOK)
}

func main(){
	reg, err := regexp.Compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if err != nil {
		log.Fatal(err)
	}
	service := UserService{
		users: make(map[string]string),
		mutex: sync.Mutex{},
		reg: reg,
	}

	http.HandleFunc("/user", service.Create)
	if err := http.ListenAndServe(":5000", nil); err != nil {
		log.Fatal(err)
	}
}