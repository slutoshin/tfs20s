package main

import (
	"bytes"
	"math/rand"
	"net/http"
	"encoding/json"
	"time"
	"log"
)

var emails = []string{"tfs2020@gmail.com", "abcd@gmail_yahoo.com", "abcd@gmail-yahoo.com", "email@example.com",
	"firstname-lastname@example.com", "email.example.com", "email@111.222.333.44444"}

type User struct{
	Email string `json:"email"`
	Password string `json:"password"`
}

func Do(){
	email := emails[rand.Intn(len(emails))]
	user := User{email, "password"}
	body, err := json.Marshal(user)
	if err != nil {
		log.Printf("%s", err.Error())
	}
	_, err = http.Post("http://127.0.0.1:5000/user","application/json", bytes.NewReader(body))
	if err != nil {
		log.Printf("%s", err.Error())
	}
}

func main(){
	rand.Seed(time.Now().Unix())
	gorotNum := 10
	for i := 0; i < 10; i++ {
		for j := 0; j < gorotNum; j++ {
			go Do()
		}
	
		time.Sleep(time.Second*1)
	}
}