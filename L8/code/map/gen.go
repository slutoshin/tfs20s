package main

import(
	"os"
	"fmt"
	"encoding/csv"
	"math/rand"
	"time"
)

var instruments = []string{"AAPL", "AFLT", "VTBR", "TCS", "CSCO", "NFLX", "GAZP",
	"YNDX", "MSFT", "AMZN"}

func main(){
	rand.Seed(time.Now().Unix())
	file, _ := os.Create("prices.csv")
	writer := csv.NewWriter(file)
	for i := 0; i < 1000000; i++ {
		instrument := instruments[rand.Intn(len(instruments))]
		price := rand.NormFloat64() + 10.6
		writer.Write([]string{instrument, fmt.Sprintf("%f", price)})
	}
}