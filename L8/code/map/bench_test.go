package bench

import (
	"testing"
	"sync"
)

func BenchmarkGoMapMutex(b *testing.B) {
	m := &MapMutex{
		mutex: sync.RWMutex{},
		prices: make(map[string]float64),
	}
	for i := 0; i < b.N; i++ {
		GoMapMutex(m)
	}
}

func BenchmarkSyncMap(b *testing.B) {
	m := sync.Map{}
	for i := 0; i < b.N; i++ {
		GoSyncMap(&m)
	}
}