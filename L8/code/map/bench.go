package bench

import(
	"os"
	"io"
	"strconv"
	"sync"
	"log"
	"encoding/csv"
	"math/rand"
	"time"
)

func init(){
	rand.Seed(time.Now().Unix())
}

type Price struct {
	InstrumentID string
	Price float64
}

type MapMutex struct{
	mutex sync.RWMutex
	prices map[string]float64
}

func(m *MapMutex) Set(instrument string, price float64) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	m.prices[instrument] = price
}

func(m *MapMutex) Get(instrument string) (float64,bool) {
	m.mutex.RLock()
	price, ok := m.prices[instrument]
	m.mutex.RUnlock()
	
	return price, ok
}

var instruments = []string{"AAPL", "AFLT", "VTBR", "TCS", "CSCO", "NFLX", "GAZP",
	"YNDX", "MSFT", "AMZN"}

var prices = read()

func read()([]Price){
	file, err := os.Create("prices.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	reader := csv.NewReader(file)
	result := make([]Price, 0)
	for {
		line, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				return result
			}
			log.Fatal(err)
		}

		price, err :=  strconv.ParseFloat(line[1], 64)
		if err != nil {
			log.Fatal(err)
		}
		result = append(result, Price{line[0], price})
	}
}

func GoMapMutex(m *MapMutex) {
	done := make(chan bool)

	go func(){
		ticker := time.NewTicker(time.Millisecond * 3)
		sum := 0.0
		for {
			select{
			case <- ticker.C:
				instrument := instruments[rand.Intn(len(instruments))]
				price, ok := m.Get(instrument)
				if ok {
					sum += price
				}
			case <- done:
				return	
			}
		}
	}()

	for _, q := range prices {
		m.Set(q.InstrumentID, q.Price)
	}

	done <- true
}


func GoSyncMap(m *sync.Map) {
	done := make(chan bool)

	go func(){
		ticker := time.NewTicker(time.Millisecond * 3)
		sum := 0.0
		for {
			select{
			case <- ticker.C:
				instrument := instruments[rand.Intn(len(instruments))]
				price, ok := m.Load(instrument)
				if ok {
					sum += price.(float64)
				}
			case <- done:
				return	
			}
		}
	}()

	for _, q := range prices {
		m.Store(q.InstrumentID, q.Price)
	}

	done <- true
}