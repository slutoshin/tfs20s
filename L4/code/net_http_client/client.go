package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"time"
)

// global variable that can be set using -ldflags parameter
// example: `go run -ldflags "-X main.debug=false" main.go`
var debug = true

func DebugPut() {
	client := &http.Client{Timeout: time.Second * 5}
	putData := []byte(`{"name":"Stefa"}`)
	req, err := http.NewRequest("PUT", "https://postman-echo.com/put", bytes.NewBuffer(putData))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("Content-type", "application/json")

	if debug {
		debugReq, err := httputil.DumpRequestOut(req, true)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%s\n", debugReq)
	}

	resp, err := client.Do(req)
	defer resp.Body.Close()

	if debug {
		debugResp, err := httputil.DumpResponse(resp, true)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%s\n", debugResp)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", body)
}

func Put() {
	client := &http.Client{Timeout: time.Second * 5}
	putData := []byte(`{"name":"Stefa"}`)
	req, err := http.NewRequest(http.MethodPut, "https://postman-echo.com/put", bytes.NewBuffer(putData))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("Content-type", "application/json")

	resp, err := client.Do(req)
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", body)
}

func main() {
	Put()
	DebugPut()
}
