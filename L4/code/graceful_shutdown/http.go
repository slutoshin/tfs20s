package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func Greeting(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	switch r.Method {
	case "GET":
		w.Write([]byte("method get implementation"))
	case "POST":
		w.Write([]byte("method post implementation"))
	default:
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte("method not implemented"))
	}
}

func main() {
	addr := net.JoinHostPort("", "5000")
	srv := &http.Server{Addr: addr}
	http.HandleFunc("/", Greeting)

	sigquit := make(chan os.Signal, 1)
	signal.Ignore(syscall.SIGHUP, syscall.SIGPIPE)
	signal.Notify(sigquit, syscall.SIGINT, syscall.SIGTERM)
	stopAppCh := make(chan struct{})
	go func() {
		s := <-sigquit
		fmt.Printf("captured signal: %v\n", s)
		fmt.Println("gracefully shutting down server")
		if err := srv.Shutdown(context.Background()); err != nil {
			log.Fatalf("could not shutdown server: %s", err)
		}

		fmt.Println("server stopped")
		stopAppCh <- struct{}{}
	}()

	fmt.Printf("starting server, listening on %s\n", addr)
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		log.Fatal(err)
	}
	<-stopAppCh
}
