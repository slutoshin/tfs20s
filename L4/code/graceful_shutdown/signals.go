package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	fmt.Println("Started application")

	stopAppCh := make(chan struct{})
	sigquit := make(chan os.Signal, 1)
	signal.Ignore(syscall.SIGHUP, syscall.SIGPIPE)
	signal.Notify(sigquit, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		s := <-sigquit
		fmt.Printf("captures signal: %v\n", s)
		fmt.Println("graceful shutdown initiated")

		// here we can gracefully close resources

		stopAppCh <- struct{}{}
	}()

	<-stopAppCh
}
