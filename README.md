#  Tinkoff Fintech School 2020, Spring

[L1.pdf](L1/L1.pdf)   
[L2.pdf](L2/L2.pdf)   
[L3.pdf](L3/L3.pdf)   
[L4.pdf](L4/L4.pdf)   
[L5.pdf](L5/L5.pdf)   
[L6.pdf](L6/L6.pdf)   
[L7.pdf](L7/L7.pdf)   
[L8.pdf](L8/L8.pdf)   
[L9.pdf](L9/L9.pdf)   

[Настройка линтера](linter.md)   
[Требования к проекту](project-requirements.md)   
[Техническое задание к проекту "ярмарка торговых роботов"](project.md)