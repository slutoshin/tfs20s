// Code generated by protoc-gen-go. DO NOT EDIT.
// source: orders/orderspb/OrdersService.proto

package orderspb

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type OrderRequest struct {
	Price                float64  `protobuf:"fixed64,1,opt,name=price,proto3" json:"price,omitempty"`
	Quantity             int64    `protobuf:"varint,2,opt,name=quantity,proto3" json:"quantity,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *OrderRequest) Reset()         { *m = OrderRequest{} }
func (m *OrderRequest) String() string { return proto.CompactTextString(m) }
func (*OrderRequest) ProtoMessage()    {}
func (*OrderRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_38f0678a661e4e16, []int{0}
}

func (m *OrderRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_OrderRequest.Unmarshal(m, b)
}
func (m *OrderRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_OrderRequest.Marshal(b, m, deterministic)
}
func (m *OrderRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_OrderRequest.Merge(m, src)
}
func (m *OrderRequest) XXX_Size() int {
	return xxx_messageInfo_OrderRequest.Size(m)
}
func (m *OrderRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_OrderRequest.DiscardUnknown(m)
}

var xxx_messageInfo_OrderRequest proto.InternalMessageInfo

func (m *OrderRequest) GetPrice() float64 {
	if m != nil {
		return m.Price
	}
	return 0
}

func (m *OrderRequest) GetQuantity() int64 {
	if m != nil {
		return m.Quantity
	}
	return 0
}

type OrdersResponse struct {
	ExecutedOrders       int64    `protobuf:"varint,1,opt,name=executedOrders,proto3" json:"executedOrders,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *OrdersResponse) Reset()         { *m = OrdersResponse{} }
func (m *OrdersResponse) String() string { return proto.CompactTextString(m) }
func (*OrdersResponse) ProtoMessage()    {}
func (*OrdersResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_38f0678a661e4e16, []int{1}
}

func (m *OrdersResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_OrdersResponse.Unmarshal(m, b)
}
func (m *OrdersResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_OrdersResponse.Marshal(b, m, deterministic)
}
func (m *OrdersResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_OrdersResponse.Merge(m, src)
}
func (m *OrdersResponse) XXX_Size() int {
	return xxx_messageInfo_OrdersResponse.Size(m)
}
func (m *OrdersResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_OrdersResponse.DiscardUnknown(m)
}

var xxx_messageInfo_OrdersResponse proto.InternalMessageInfo

func (m *OrdersResponse) GetExecutedOrders() int64 {
	if m != nil {
		return m.ExecutedOrders
	}
	return 0
}

func init() {
	proto.RegisterType((*OrderRequest)(nil), "lots.OrderRequest")
	proto.RegisterType((*OrdersResponse)(nil), "lots.OrdersResponse")
}

func init() {
	proto.RegisterFile("orders/orderspb/OrdersService.proto", fileDescriptor_38f0678a661e4e16)
}

var fileDescriptor_38f0678a661e4e16 = []byte{
	// 191 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x52, 0xce, 0x2f, 0x4a, 0x49,
	0x2d, 0x2a, 0xd6, 0x87, 0x50, 0x05, 0x49, 0xfa, 0xfe, 0x60, 0x46, 0x70, 0x6a, 0x51, 0x59, 0x66,
	0x72, 0xaa, 0x5e, 0x41, 0x51, 0x7e, 0x49, 0xbe, 0x10, 0x4b, 0x4e, 0x7e, 0x49, 0xb1, 0x92, 0x03,
	0x17, 0x0f, 0x58, 0x32, 0x28, 0xb5, 0xb0, 0x34, 0xb5, 0xb8, 0x44, 0x48, 0x84, 0x8b, 0xb5, 0xa0,
	0x28, 0x33, 0x39, 0x55, 0x82, 0x51, 0x81, 0x51, 0x83, 0x31, 0x08, 0xc2, 0x11, 0x92, 0xe2, 0xe2,
	0x28, 0x2c, 0x4d, 0xcc, 0x2b, 0xc9, 0x2c, 0xa9, 0x94, 0x60, 0x52, 0x60, 0xd4, 0x60, 0x0e, 0x82,
	0xf3, 0x95, 0x2c, 0xb8, 0xf8, 0x20, 0xc6, 0x07, 0xa5, 0x16, 0x17, 0xe4, 0xe7, 0x15, 0xa7, 0x0a,
	0xa9, 0x71, 0xf1, 0xa5, 0x56, 0xa4, 0x26, 0x97, 0x96, 0xa4, 0xa6, 0x40, 0x64, 0xc0, 0x86, 0x31,
	0x07, 0xa1, 0x89, 0x1a, 0x79, 0x71, 0xf1, 0xa2, 0x38, 0x4c, 0xc8, 0x92, 0x8b, 0x33, 0x20, 0xbf,
	0xb8, 0x04, 0x2c, 0x28, 0x24, 0xa4, 0x07, 0x72, 0xa0, 0x1e, 0xb2, 0xeb, 0xa4, 0x44, 0x90, 0xc4,
	0xe0, 0xf6, 0x29, 0x31, 0x68, 0x30, 0x3a, 0x71, 0x45, 0x71, 0xc0, 0x7c, 0x9b, 0xc4, 0x06, 0xf6,
	0xa0, 0x31, 0x20, 0x00, 0x00, 0xff, 0xff, 0xef, 0xe7, 0x40, 0x6c, 0x07, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// OrdersServiceClient is the client API for OrdersService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type OrdersServiceClient interface {
	PostOrder(ctx context.Context, opts ...grpc.CallOption) (OrdersService_PostOrderClient, error)
}

type ordersServiceClient struct {
	cc *grpc.ClientConn
}

func NewOrdersServiceClient(cc *grpc.ClientConn) OrdersServiceClient {
	return &ordersServiceClient{cc}
}

func (c *ordersServiceClient) PostOrder(ctx context.Context, opts ...grpc.CallOption) (OrdersService_PostOrderClient, error) {
	stream, err := c.cc.NewStream(ctx, &_OrdersService_serviceDesc.Streams[0], "/lots.OrdersService/PostOrder", opts...)
	if err != nil {
		return nil, err
	}
	x := &ordersServicePostOrderClient{stream}
	return x, nil
}

type OrdersService_PostOrderClient interface {
	Send(*OrderRequest) error
	CloseAndRecv() (*OrdersResponse, error)
	grpc.ClientStream
}

type ordersServicePostOrderClient struct {
	grpc.ClientStream
}

func (x *ordersServicePostOrderClient) Send(m *OrderRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *ordersServicePostOrderClient) CloseAndRecv() (*OrdersResponse, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(OrdersResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// OrdersServiceServer is the server API for OrdersService service.
type OrdersServiceServer interface {
	PostOrder(OrdersService_PostOrderServer) error
}

// UnimplementedOrdersServiceServer can be embedded to have forward compatible implementations.
type UnimplementedOrdersServiceServer struct {
}

func (*UnimplementedOrdersServiceServer) PostOrder(srv OrdersService_PostOrderServer) error {
	return status.Errorf(codes.Unimplemented, "method PostOrder not implemented")
}

func RegisterOrdersServiceServer(s *grpc.Server, srv OrdersServiceServer) {
	s.RegisterService(&_OrdersService_serviceDesc, srv)
}

func _OrdersService_PostOrder_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(OrdersServiceServer).PostOrder(&ordersServicePostOrderServer{stream})
}

type OrdersService_PostOrderServer interface {
	SendAndClose(*OrdersResponse) error
	Recv() (*OrderRequest, error)
	grpc.ServerStream
}

type ordersServicePostOrderServer struct {
	grpc.ServerStream
}

func (x *ordersServicePostOrderServer) SendAndClose(m *OrdersResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *ordersServicePostOrderServer) Recv() (*OrderRequest, error) {
	m := new(OrderRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

var _OrdersService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "lots.OrdersService",
	HandlerType: (*OrdersServiceServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "PostOrder",
			Handler:       _OrdersService_PostOrder_Handler,
			ClientStreams: true,
		},
	},
	Metadata: "orders/orderspb/OrdersService.proto",
}
