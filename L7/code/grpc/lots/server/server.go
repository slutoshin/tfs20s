package main

import (
	"log"
	"math/rand"
	"net"
	"tfs20s/L7/code/grpc/lots/lotspb"
	"time"

	"google.golang.org/grpc"
)

type server struct{}

func (s *server) ActiveLots(req *lotspb.LotsRequest, resp lotspb.LotsService_ActiveLotsServer) error {
	startPrice := rand.Intn(100)
	for i := 1; i < 11; i++ {
		res := lotspb.LotsResponse{
			Lot: &lotspb.Lot{
				ID:    int64(i),
				Desc:  "Description",
				Price: float64(startPrice * i),
			},
		}
		resp.Send(&res)
		time.Sleep(time.Second * 3)
		if req.Limit < int64(i)+1 {
			break
		}
	}
	return nil
}

func main() {
	listen, err := net.Listen("tcp", ":5000")
	if err != nil {
		log.Fatalf("can't listen on port: %v", err)
	}

	s := grpc.NewServer()
	lotspb.RegisterLotsServiceServer(s, &server{})
	if err := s.Serve(listen); err != nil {
		log.Fatalf("can't register service server: %v", err)
	}
}
