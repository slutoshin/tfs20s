package main

import (
	"context"
	"log"
	"net"
	"tfs20s/L7/code/grpc/login/loginpb"

	"google.golang.org/grpc"
)

type server struct{}

func (s *server) Login(ctx context.Context, req *loginpb.LoginRequest) (*loginpb.LoginResponse, error) {
	if req.Login == "login" && req.Password == "passwd" {
		res := loginpb.LoginResponse{Result: "Ok"}
		return &res, nil
	}
	res := loginpb.LoginResponse{Result: "Error"}
	return &res, nil
}

func main() {
	listen, err := net.Listen("tcp", ":5000")
	if err != nil {
		log.Fatalf("can't listen on port: %v", err)
	}

	s := grpc.NewServer()
	loginpb.RegisterLoginServiceServer(s, &server{})
	if err := s.Serve(listen); err != nil {
		log.Fatalf("can't register service server: %v", err)
	}
}
